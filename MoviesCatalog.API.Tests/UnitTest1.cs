using System;
using Microsoft.AspNetCore.Mvc;
using MovieCatalog.API.Controllers;
using Xunit;

namespace MoviesCatalog.API.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test_NotNull_HomeController_Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            var result = controller.Index();

            // Assert
            Assert.NotNull(result);
        }
    }
}
